package br.pucrs.inf.fuelmanager.persistence;


public class DataAccessException extends Exception {

	public DataAccessException() {
		super();
	}
	
	public DataAccessException(String message) {
		super(message);
	}
	
	public DataAccessException(String message, Exception e) {
		super(message, e);
	}
}
