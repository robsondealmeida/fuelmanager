package br.pucrs.inf.fuelmanager.persistence;

import java.sql.*;
import java.util.Map;

public class BaseDAO {

		
	public BaseDAO() throws Exception {
		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
		} catch (ClassNotFoundException e) {
			throw new DataAccessException("Driver para conexão com o banco não encontrado.", e);
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	public BaseDAO(boolean createDatabase) throws Exception {
		
		this();
		
		try {
			this.createDatabase();
		} catch (Exception e) {
			throw new DataAccessException("Falha na criação do banco de dados.", e);
		}
	}


	private void createDatabase() {
		// TODO
	}


	
	
	public void execute(String sql) throws DataAccessException {
		
		Connection connection = null;
		Statement statement = null;
		
		try {
			
			connection = this.openConnection();
			statement = connection.createStatement();		
			
			System.out.println(sql);
			
			statement.executeUpdate(sql);
			
			
		} catch (SQLException e) {
			throw new DataAccessException(
					"Falha na execução do comando. " + e.getMessage(), e);
		} finally {
			try {
				
				if (statement != null)
					statement.close();
				
				if (connection != null)
					connection.close();
				
			} catch (SQLException e) {
				throw new DataAccessException(
						"Falha ao encerrar a conexão com o banco de dados", e);
			}
		}
	}


	protected Connection openConnection() throws DataAccessException {
        
		try {
			return DriverManager.getConnection("jdbc:derby:db");
		} catch (SQLException e) {
			throw new DataAccessException(
					"Falha ao abrir a conexão com o banco de dados. " + e.getMessage(), e);
		}
	}
}
