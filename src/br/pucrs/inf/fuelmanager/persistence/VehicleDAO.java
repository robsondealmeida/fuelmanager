package br.pucrs.inf.fuelmanager.persistence;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.pucrs.inf.fuelmanager.domain.IVehicleDAO;
import br.pucrs.inf.fuelmanager.domain.Loader;
import br.pucrs.inf.fuelmanager.domain.VehicleDTO;
import br.pucrs.inf.fuelmanager.domain.VehicleLoader;

public class VehicleDAO extends BaseDAO implements IVehicleDAO {

	public VehicleDAO() throws Exception {
		super();
	}

	@Override
	public void save(VehicleDTO vehicleDto) throws DataAccessException {
		
		VehicleDTO other = this.find(vehicleDto.getLicense());
		
		
		String sql = "";
		
		if (other == null) {
			
			sql = "insert into FUELMANAGER.VEICULOS values " + 
							"('" + 
								vehicleDto.getLicense() + "', '" +
								vehicleDto.getOwner() + "', '" +
								vehicleDto.getModel() + "', " +
								vehicleDto.getYear() + ", '" +
								vehicleDto.getManufacturer() + "', " +
								vehicleDto.getOdometer() +
							")";
			
			this.execute(sql);
			
		} else {
			
			sql = "update FUELMANAGER.VEICULOS set " + 
					"OWNER = '" + vehicleDto.getOwner() + "', " +
					"MODEL = '" + vehicleDto.getModel() + "', " + 
					"ANO = " + vehicleDto.getYear() + ", " + 
					"MANUFACTURER = '" + vehicleDto.getManufacturer() + "', " + 
					"ODOMETER = " + vehicleDto.getOdometer() + 
					" where LICENSE = '" + vehicleDto.getLicense() + "'";
			
			
			this.execute(sql);
		}
	}

	

	@Override
	public VehicleDTO find(String license) throws DataAccessException {
		
		String sql = 
				"select * from FUELMANAGER.VEICULOS where LICENSE = '" + license + "'";
		
		
		Connection connection = null;
		Statement statement = null;
		ResultSet set = null;
		
		try {
			
			connection = this.openConnection();
			statement = connection.createStatement();
			
			set = statement.executeQuery(sql);
			
			
			while(set != null && set.next()) {
				
				String model = set.getString("MODEL");
				String owner = set.getString("OWNER");
				int year = set.getInt("ANO");
				String manufacturer = set.getString("MANUFACTURER");
				long odometer = set.getLong("ODOMETER");
				
				return new VehicleDTO(owner, license, model, year, manufacturer, odometer);
			}
			
		} catch (SQLException e) {
			throw new DataAccessException(
					"Falha na recuperação dos resultados. " + e.getMessage(), e);
		} finally {
			try {
				
				if (set != null)
					set.close();
				
				if (statement != null)
					statement.close();
				
				if (connection != null)
					connection.close();
				
			} catch (SQLException e) {
				throw new DataAccessException(
						"Falha ao encerrar a conexão com o banco de dados", e);
			}
		}
		
		return null;
	}

	@Override
	public List<VehicleDTO> find() throws DataAccessException {
		
		String sql = "select * from FUELMANAGER.VEICULOS";
		
		
		List<VehicleDTO> vehicles = new ArrayList<VehicleDTO>();
		
		Connection connection = null;
		Statement statement = null;
		ResultSet set = null;
		
		try {
			
			connection = this.openConnection();
			statement = connection.createStatement();
			
			set = statement.executeQuery(sql);
			
			
			while(set.next()) {
				
				String license = set.getString("LICENSE"); 
				String model = set.getString("MODEL");
				String owner = set.getString("OWNER");
				int year = set.getInt("ANO");
				String manufacturer = set.getString("MANUFACTURER");
				long odometer = set.getLong("ODOMETER");
				
				vehicles.add(new VehicleDTO(owner, license, model, year, manufacturer, odometer));
			}
			
		} catch (SQLException e) {
			throw new DataAccessException(
					"Falha na recuperação dos resultados. " + e.getMessage(), e);
		} finally {
			try {
				
				if (set != null)
					set.close();
				
				if (statement != null)
					statement.close();
				
				if (connection != null)
					connection.close();
				
			} catch (SQLException e) {
				throw new DataAccessException(
						"Falha ao encerrar a conexão com o banco de dados", e);
			}
		}
		
		
		
		return vehicles;
	}

	@Override
	public void delete(String license) {
		// TODO Auto-generated method stub

	}

}
