package br.pucrs.inf.fuelmanager.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface Loader<T> {

	T load(ResultSet set) throws SQLException;
	List<T> loadMany(ResultSet set) throws SQLException;
}
