package br.pucrs.inf.fuelmanager.domain;

public class VehicleDTO {

	private String owner;
	private String license;
	private String model;
	private int year;
	private String manufacturer;
	private long odometer;
	
	
	public VehicleDTO(String owner, String license, String model,
			int year, String manufacturer, long odometer) {
		
		
		this.owner = owner;
		this.license = license;
		this.model = model;
		this.year = year;
		this.manufacturer = manufacturer;
		this.odometer = odometer;
	}
	
	
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getLicense() {
		return license;
	}
	public void setLicense(String license) {
		this.license = license;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public long getOdometer() {
		return odometer;
	}
	public void setOdometer(long odometer) {
		this.odometer = odometer;
	}
}
