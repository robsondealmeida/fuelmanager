package br.pucrs.inf.fuelmanager.domain;

import java.util.ArrayList;
import java.util.List;

import br.pucrs.inf.fuelmanager.persistence.DataAccessException;
import br.pucrs.inf.fuelmanager.persistence.VehicleDAO;

public class FuelManagerFacade {
	
	private IVehicleDAO vehicleDao;
	
	
	
	
	public FuelManagerFacade() throws PersistVehicleException {
		try {
			this.vehicleDao = new VehicleDAO();
		} catch (Exception e) {
			throw new PersistVehicleException(
					"Erro de comunicação: " + e.getMessage());
		}
	}
	
	

	public void saveVehicle(String owner, String license, String model,
			Integer year, String manufacturer, Long odometer) throws PersistVehicleException {
		
		
		Vehicle vehicle = new Vehicle(new Owner(owner), license, model, year, 
								new Manufacturer(manufacturer), odometer);
		
		
		VehicleDTO dto = VehicleMapper.map(vehicle);
		
		
		try {
			this.vehicleDao.save(dto);
		} catch (DataAccessException e) {
			throw new PersistVehicleException(
					"Falha ao salvar o veículo: " + e.getMessage());
		}
	}



	public List<Vehicle> findVehicles() throws PersistVehicleException {
		
		
		try {
			
			List<VehicleDTO> items = this.vehicleDao.find();
			
			
			List<Vehicle> vehicles = new ArrayList<Vehicle>();

			for (VehicleDTO dto : items)
				vehicles.add(VehicleMapper.map(dto));
			
			
			return vehicles;
			
		} catch (DataAccessException e) {
			throw new PersistVehicleException(
					"Falha na listagem dos veículos. " + e.getMessage());
		}
	}



	public Vehicle findVehicle(String license) throws PersistVehicleException {
		
		VehicleDTO vehicle;
		
		try {
			
			vehicle = this.vehicleDao.find(license);
			
			
		} catch (DataAccessException e) {
			throw new PersistVehicleException(
					"Falha na seleção do veículo. " + e.getMessage());
		}
		
		return VehicleMapper.map(vehicle);
	}
}
