package br.pucrs.inf.fuelmanager.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class VehicleLoader implements Loader<VehicleDTO> {

	@Override
	public VehicleDTO load(ResultSet set) throws SQLException {
	
		if (set == null)
			return null;
		
		

		while(set.next()) {
			
			String license = set.getString("LICENSE"); 
			String model = set.getString("MODEL");
			String owner = set.getString("OWNER");
			int year = set.getInt("ANO");
			String manufacturer = set.getString("MANUFACTURER");
			long odometer = set.getLong("ODOMETER");
			
			return new VehicleDTO(owner, license, model, year, manufacturer, odometer);
		}
		
		
		return null;
	}
	
	@Override
	public List loadMany(ResultSet set) throws SQLException {
		
		if (set == null)
			return null;
		
		
		List<VehicleDTO> vehicles = new ArrayList<VehicleDTO>();
		
		while(set.next()) {
			
			String license = set.getString("LICENSE"); 
			String model = set.getString("MODEL");
			String owner = set.getString("OWNER");
			int year = set.getInt("ANO");
			String manufacturer = set.getString("MANUFACTURER");
			long odometer = set.getLong("ODOMETER");
			
			vehicles.add(new VehicleDTO(owner, license, model, year, manufacturer, odometer));
		}
		
		
		
		return vehicles;
	}
}
