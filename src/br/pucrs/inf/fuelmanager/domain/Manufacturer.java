package br.pucrs.inf.fuelmanager.domain;

public class Manufacturer {

	
	private String name;
	
	
	public Manufacturer() {
	
	}
	
	public Manufacturer(String name) {
		
		super();
		
		this.name = name;
	}
	
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	@Override
	public String toString() {
		return this.name;
	}
	
}
