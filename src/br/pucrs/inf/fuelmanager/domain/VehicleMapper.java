package br.pucrs.inf.fuelmanager.domain;

public class VehicleMapper {

	public static VehicleDTO map(Vehicle vehicle) {
		
		if (vehicle == null)
			return null;
		
		
		return new VehicleDTO(
					vehicle.getOwner().getName(),
					vehicle.getLicense(),
					vehicle.getModel(),
					vehicle.getYear(),
					vehicle.getManufacturer().getName(),
					vehicle.getOdometer()
				);
	}

	public static Vehicle map(VehicleDTO dto) {
		
		if (dto == null)
			return null;
		
		return new Vehicle(
					new Owner(dto.getOwner()),
					dto.getLicense(), dto.getModel(), dto.getYear(),
					new Manufacturer(dto.getManufacturer()),
					dto.getOdometer()
				);
	}
}
