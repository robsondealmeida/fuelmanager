package br.pucrs.inf.fuelmanager.domain;

public class PersistVehicleException extends Exception {

	public PersistVehicleException() {
		super();
	}
	
	public PersistVehicleException(String message) {
		super(message);
	}
}
