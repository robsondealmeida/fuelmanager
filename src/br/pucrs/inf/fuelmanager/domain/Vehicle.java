package br.pucrs.inf.fuelmanager.domain;

public class Vehicle {

	

	private Owner owner;
	private String license;
	private String model;
	private Integer year;
	private Manufacturer manufacturer;
	private Long odometer;
	

	public Vehicle() {
		
	}

	public Vehicle(Owner owner, String license, String model, int year,
			Manufacturer manufacturer, long odometer) {
		
		super();
		
		this.owner = owner;
		this.license = license;
		this.model = model;
		this.year = year;
		this.manufacturer = manufacturer;
		this.odometer = odometer;
	}
	


	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Manufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Long getOdometer() {
		return odometer;
	}

	public void setOdometer(Long odometer) {
		this.odometer = odometer;
	}
}
