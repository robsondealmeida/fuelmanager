package br.pucrs.inf.fuelmanager.domain;

import java.util.List;

import br.pucrs.inf.fuelmanager.persistence.DataAccessException;

public interface IBaseDAO<T, E> {

	
	void save(T object) throws DataAccessException;
	T find(E identifier) throws DataAccessException;
	List<T> find() throws DataAccessException;
	void delete(E identifier);
}
