package controllers;

import play.*;
import play.data.validation.*;
import play.mvc.*;
import play.mvc.results.*;
import java.util.*;

import br.pucrs.inf.fuelmanager.domain.FuelManagerFacade;
import br.pucrs.inf.fuelmanager.domain.Manufacturer;
import br.pucrs.inf.fuelmanager.domain.Owner;
import br.pucrs.inf.fuelmanager.domain.Vehicle;
import br.pucrs.inf.fuelmanager.domain.VehicleMapper;

import models.VehicleViewModel;


public class VehicleController extends Controller {

	
	@Before
	static void clearFlash() {
		flash.clear();
	}
	
	
	
	public static void index() {
		
		try {
			
			FuelManagerFacade fuelManagerFacade = new FuelManagerFacade();
			
			
			List<Vehicle> items = fuelManagerFacade.findVehicles();
			
			
			
			List<VehicleViewModel> vehicles = new ArrayList<VehicleViewModel>();
			
			for (Vehicle vehicle : items)
				vehicles.add(
							new VehicleViewModel(
									vehicle.getLicense(), 
									vehicle.getModel(), 
									vehicle.getYear(), 
									vehicle.getManufacturer().getName(), 
									2.0)
							);
			
			
			
			renderTemplate("vehicle/index.html", vehicles);
			
		} catch (Exception e) {
			e.printStackTrace();
			flash.error("" + e.getMessage(), e.getMessage());
		}
		
		renderTemplate("vehicle/index.html");
	}
	
	
	
	public static void show(String license) {
		
	}
	
	
	public static void create(
				@Required(message="Proprietário é obrigatório.") 
				String owner,
				
				@Required(message="Placa é obrigatório.") 
				String license,
				
				@Required(message="Modelo é obrigatório.") 
				String model,
				
				@Required(message="Ano é obrigatório.") 
				@Min(value=1900, message="O ano deve ser superior a 1900") 
				Integer year,
				
				@Required(message="Fabricante é obrigatório.") 
				String manufacturer,
				
				@Required(message="Odômetro é obrigatório.") 
				@Min(value=0, message="O odômetro não pode ser negativo.") 
				Long odometer
			) {

		
		
		if(!validation.hasErrors()) {
			
			try {
				FuelManagerFacade fuelManagerFacade = new FuelManagerFacade();
				fuelManagerFacade.saveVehicle(owner, license, model, year, manufacturer, odometer);
				
				flash.success("Veículo %s salvo com sucesso.", license);
				
			} catch (Exception e) {
				e.printStackTrace();
				flash.error("" + e.getMessage(), e.getMessage());
			}
		}
		
		
			
		
		renderTemplate("vehicle/form.html");
	}


	public static void edit(String license) {
		
		
		Vehicle vehicle = null;
		
		try {
			
			vehicle = new FuelManagerFacade().findVehicle(license);
			
		} catch (Exception e) {
			e.printStackTrace();
			flash.error("" + e.getMessage(), e.getMessage());
		}
		
		renderTemplate("vehicle/form.html", vehicle);
	}
}
