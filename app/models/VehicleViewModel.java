package models;

public class VehicleViewModel {

	
	public String license;
	public String model;
	public int year;
	public String manufacturer;
	public double consumption;
	
	
	public VehicleViewModel(String license, String model, int year,
			String manufacturer, double consumption) {
		
		this.license = license;
		this.model = model;
		this.year = year;
		this.manufacturer = manufacturer;
		this.consumption = consumption;		
	}
}
